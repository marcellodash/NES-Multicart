  .INESPRG 16
  .INESCHR 0
  .INESMAP 1
  .INESMIR 0


  .rsset $0000
Zero: .rs 1
ZeroWrite: .rs 1
Frame: .rs 1
DecompressDataPointer: .rs 2
CompressedDataFlag: .rs 1
MapperSetupRAM: .rs 1
GameBankRAM: .rs 1
GameSelectedRAM: .rs 1
Cursor: .rs 1
ControllerData: .rs 1
ControllerDataOld: .rs 1
ControllerDataNewlyPressed: .rs 1

  .include "Program/Banks/Bank0.asm"
  .include "Program/Banks/Bank31.asm"

PPUCtrl=$2000;
PPUMask=$2001;
PPUStatus=$2002;
PPUOamAddr=$2003;
PPUOamData=$2004;
PPUScroll=$2005;
PPUAddr=$2006;
PPUData=$2007;
PPUDma=$4014;
ControllerOutput=$4016;
UpButton=$08;
DownButton=$04;
LeftButton=$02;
RightButton=$01;
AButton=$80;
BButton=$40;
SelectButton=$20;
StartButton=$10;