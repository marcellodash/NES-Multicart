ProgramDataBank: ;16KB/32KB aligned banks for the program. Must be on 16KB aligned if a 16KB game, and on a 32KB if a 32KB game. LSR by one so it is aligned to the 16KB program page value. .db BANK(GameProgram)>>1
  .db BANK(AlterEgoPgm)>>1
  .db BANK(BombSweeperPgm)>>1
  .db BANK(ConcentrationRoomPgm)>>1
  .db BANK(LawnMowerPgm)>>1
  .db BANK(ThwaitePgm)>>1
  .db BANK(VirusCleanerPgm)>>1
  .db BANK(ZoomingSecretaryPgm)>>1

GraphicDataBank: ;8KB bank value. .db BANK(GameCharacter)

  .db BANK(AlterEgoChr)
  .db BANK(BombSweeperChr)
  .db BANK(ConcentrationRoomChr)
  .db BANK(LawnMowerChr)
  .db BANK(ThwaiteChr)
  .db BANK(VirusCleanerChr)
  .db BANK(ZoomingSecretaryChr)

GameSizes: ;0=32KB, 1=16KB.

  .db $00
  .db $01
  .db $01
  .db $01
  .db $01
  .db $00
  .db $00


GameMirroring: ;Bottom 2 bits of MMC1Ctrl register. 00=One screen lower. 01=One screen upper. 02=Vertical mirroring. 03=Horizontal mirroring.

  .db $02
  .db $03
  .db $02
  .db $02
  .db $02
  .db $03
  .db $03


NumberOfGames=7 ;Starts at 1. Add to each time you add a game.