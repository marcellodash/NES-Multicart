NES NROM Multicart

Runs NROM NES games injected into it. Selectable from a menu. Can hold up to 10 NROM-128 (24KB) games. Uses CHR-RAM cart, so only one EPROM is needed.

Compiles with NESASM3 on Program/Program.asm.

Todo:

Dynamic names from the "GameChangeFile.asm" instead of having to edit nametables manually.